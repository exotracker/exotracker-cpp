#pragma once

namespace doc::accidental {

/// Saved in documents. Default value is stored in global settings.
enum class AccidentalMode {
    Sharp,
    Flat,
};

}
