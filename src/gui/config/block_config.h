#pragma once

namespace gui::config::block_config {

enum class ExtendBlock {
    Never,
    Adjacent,
    Always,
};

}
