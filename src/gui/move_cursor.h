#pragma once

/// Only vertical (in time) cursor movement is implemented here.
/// Horizontal (columns/subcolumns/digits) movement is still in
/// pattern_editor_panel.cpp.

#include "cursor.h"
#include "doc.h"
#include "gui/config/cursor_config.h"
#include "timing_common.h"
#include "chip_common.h"

#include <vector>

namespace gui::move_cursor {

using timing::TickT;


// # Moving cursor by events

/// Return the timestamp of the nearest event
/// whose beat fraction is strictly less than the cursor's.
///
/// May wrap around to the beginning of the song.
///
/// If the channel has no events, returns {original time, wrapped=true}.
[[nodiscard]]
TickT prev_event(doc::Document const& document, cursor::Cursor cursor);

/// Return the timestamp of the next event
/// whose beat fraction is strictly greater than the cursor's.
///
/// May wrap around to the beginning of the song.
///
/// If the channel has no events, returns {original time, wrapped=true}.
[[nodiscard]]
TickT next_event(doc::Document const& document, cursor::Cursor cursor);


// # Moving cursor by beats

using config::cursor_config::MovementConfig;

[[nodiscard]] TickT prev_beat(doc::Document const& doc, TickT cursor_y);

[[nodiscard]] TickT next_beat(doc::Document const& doc, TickT cursor_y);

/// Options owned by pattern editor panel and set in GUI, not set in settings dialog.
struct MoveCursorYArgs {
    int ticks_per_row;
    int step;
};

/// - If step > 1 and we follow step, move to nearest row above `step` times.
/// - If sub-row movement is enabled,
///   and prev_event() doesn't wrap and is closer than the nearest row above,
///   jump up to prev_event().time.
/// - Move to nearest row above.
[[nodiscard]] TickT move_up(
    doc::Document const& document,
    cursor::Cursor cursor,
    MoveCursorYArgs const& args,
    MovementConfig const& move_cfg
);

/// - If step > 1 and we follow step, move to nearest row below `step` times.
/// - If sub-row movement is enabled,
///   and next_event() doesn't wrap and is closer than the nearest row below,
///   jump down to next_event().time.
/// - Move to nearest row below.
[[nodiscard]] TickT move_down(
    doc::Document const& document,
    cursor::Cursor cursor,
    MoveCursorYArgs const& args,
    MovementConfig const& move_cfg
);


[[nodiscard]] TickT page_up(
    doc::Document const& document,
    TickT cursor_y,
    int ticks_per_row,
    MovementConfig const& move_cfg);

[[nodiscard]] TickT page_down(
    doc::Document const& document,
    TickT cursor_y,
    int ticks_per_row,
    MovementConfig const& move_cfg);


[[nodiscard]] TickT block_begin(
    doc::Document const& document,
    cursor::Cursor cursor,
    MovementConfig const& move_cfg);

[[nodiscard]] TickT block_end(
    doc::Document const& document,
    cursor::Cursor cursor,
    MovementConfig const& move_cfg,
    TickT bottom_padding);


[[nodiscard]] TickT prev_block(
    doc::Document const& document,
    cursor::Cursor cursor,
    TickT ticks_per_row);

[[nodiscard]] TickT next_block(
    doc::Document const& document,
    cursor::Cursor cursor,
    TickT ticks_per_row);


struct CursorStepArgs {
    TickT ticks_per_row;
    int step;
    bool step_to_event;
};

/// Called when the user types into an event and steps to a new location.
/// Returns the new vertical location of the cursor.
///
/// - If "step to event" enabled, move to the next event.
/// - If step = 1, sub-row movement is enabled,
///   and next_event() doesn't wrap and is closer than the nearest row below,
///   jump down to next_event().time.
/// - Move to nearest row below `step` times.
[[nodiscard]] TickT cursor_step(
    doc::Document const& document,
    cursor::Cursor cursor,
    CursorStepArgs const& args,
    MovementConfig const& move_cfg
);

}
